import 'package:flutter/material.dart';
import 'package:genie/src/pages/account_page.dart';
import 'package:genie/src/pages/category_page.dart';
import 'package:genie/src/pages/dashboard_page.dart';
import 'package:genie/src/pages/discounts_page.dart';
import 'package:genie/src/pages/initial_page.dart';
import 'package:genie/src/pages/loading_page.dart';
import 'package:genie/src/pages/onboarding_page.dart';
import 'package:genie/src/pages/search_page.dart';
import 'package:genie/src/pages/signin_page.dart';
import 'package:genie/src/pages/signup_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => InitialPage(),
        '/signin': (context) => SignInPage(),
        '/signup': (context) => SignUpPage(),
        '/category': (context) => CategoryPage(),
        '/onboarding': (context) => OnboardingPage(),
        '/dashboard': (context) => DashboardPage(),
        '/account': (context) => AccountPage(),
        '/discounts': (context) => DiscountsPage(),
        '/search': (context) => SearchPage(),
        '/loading': (context) => LoadingPage(),
      },
    );
  }
}
