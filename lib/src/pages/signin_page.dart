import 'package:flutter/material.dart';
import 'package:genie/src/components/standart_button_component.dart';
import 'package:genie/src/utils/colors/custom_colors.dart';

class SignInPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomColors.colorPrimaryOrange,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _title('INGRESA A TU CUENTA', size),
              _textForm(size),
              _switchCurrentSession(),
              SizedBox(
                height: size.height * 0.34,
              ),
              StandartButtonComponent(
                text: 'INGRESAR',
                color: CustomColors.colorPrimaryBlue,
                route: '/loading',
              ),
              SizedBox(
                height: size.height * 0.033,
              ),
              _signUp(),
              SizedBox(
                height: size.height * 0.033,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _title(String title, Size size) {
    return Container(
      width: double.infinity,
      height: size.height * 0.1,
      child: Center(
          child: Text(title,
              style: TextStyle(fontSize: 25.0, color: Colors.white))),
    );
  }

  Widget _textForm(Size size) {
    return Container(
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[
          _textField('EMAIL', TextInputType.emailAddress, false),
          SizedBox(
            height: size.height * 0.033,
          ),
          _textField('CONTRASEÑA', TextInputType.visiblePassword, true)
        ],
      ),
    );
  }

  Widget _textField(String text, TextInputType keyboardType, bool obscureText) {
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            text,
            style: TextStyle(fontSize: 18.0),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        TextField(
          obscureText: obscureText,
          keyboardType: keyboardType,
          decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(10.0))),
        ),
      ],
    );
  }

  Widget _switchCurrentSession() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Mantener iniciada la sesión',
          style: TextStyle(fontSize: 18.0),
        ),
        Switch(
          value: true,
          onChanged: null,
          inactiveTrackColor: Colors.blueGrey,
          inactiveThumbColor: CustomColors.colorPrimaryBlue,
        )
      ],
    );
  }

  Widget _signUp() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal:15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            '¿No estás registrado?',
            style: TextStyle(fontSize: 16.0),
          ),
          Text(
            'CREA TU CUENTA!',
            style: TextStyle(fontSize: 18.0, color: CustomColors.colorPrimaryBlue),
          ),
        ],
      ),
    );
  }
}
