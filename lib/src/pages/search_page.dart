import 'package:flutter/material.dart';
import 'package:genie/src/components/standart_button_component.dart';
import 'package:genie/src/utils/colors/custom_colors.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  final TextEditingController controller = TextEditingController();
  bool showList = true;

  @override
  Widget build(BuildContext context) {

    final Size size = MediaQuery.of(context).size;

    controller.addListener(() { 
      if(controller.text.length > 0){
        setState(() {
          showList = false;
        });
      } else {
        setState(() {
          showList = true;
        });
      }
    });

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: CustomColors.colorAppBar,
        title: Text('Beneficios'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[_searchBar(), 
        showList? _discountList(): _notFound(size) ],
      ),
    );
  }

  Widget _searchBar() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
      color: CustomColors.colorAppBar,
      child: TextField(
        controller: controller,
        obscureText: false,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
            hintText: 'Buscar local, categoría, ítem, etc.',
            prefixIcon: Icon(Icons.search),
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(10.0))),
      ),
    );
  }

  Widget _notFound(Size size) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: size.height * 0.05,
          ),
          Image(
              height: size.height * 0.3,
              image:
                  AssetImage('assets/illustrations/open-doodles-sitting.png')),
          SizedBox(
            height: size.height * 0.05,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            height: size.height * 0.06,
            child: Center(
              child: Text(
                'Parece que por el momento, no tenemos ningún beneficio',
                style: TextStyle(
                  fontSize: 18.0,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: size.height * 0.05,
          ),
          StandartButtonComponent(
            text: 'VOLVER',
            route:  '/back',
            color: CustomColors.colorPrimaryBlue,
          ),
          SizedBox(
            height: size.height * 0.1,
          ),
          Text('¿No encontraste la tienda que querías'),
          Text(
            'Informanos para que podamos sumarlo.',
            style: TextStyle(color: CustomColors.colorPrimaryOrange),
          )
        ],
      ),
    );
  }

  Widget _descuentoCard() {
    final _height = 150.0;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      margin: EdgeInsets.symmetric(vertical: 5.0),
      height: _height,
      child: Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0)),
              child: Image.asset(
                'assets/images/oferta_de_hoy.png',
                fit: BoxFit.fill,
                width: _height,
              ),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Entretenimiento'),
                  Text(
                    'Cirque du Solei',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    '20% DE DESCUENTO',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  Expanded(child: Container()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('8,1 km'),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: CustomColors.colorprimaryGrey),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text('Venc: 29/11'),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

  Widget _header() {
    return Container(

      padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 70.0,
            child: Card(
                elevation: 10.0,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.location_on,
                        color: CustomColors.colorPrimaryBlue,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        'VER EN MAPA',
                        style:
                            TextStyle(fontSize: 18.0, color: CustomColors.colorPrimaryBlue),
                      )
                    ],
                  ),
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Text('Ordernar por', style: TextStyle(fontSize: 18.0, color: CustomColors.colorPrimaryOrange),),
          )
        ],
      ),
    );
  }

  Widget _discountList() {
    return Expanded(
      child: ListView(
        
        children: <Widget>[
          _header(),
          _descuentoCard(),
          _descuentoCard(),
          _descuentoCard(),
          _descuentoCard(),
          _descuentoCard(),
          _descuentoCard(),
          _descuentoCard(),
          _descuentoCard(),
        ],
      ),
    );
  }
}
