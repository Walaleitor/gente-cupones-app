import 'package:flutter/material.dart';
import 'package:genie/src/data/data.dart';
import 'package:genie/src/utils/colors/custom_colors.dart';

class DiscountsPage extends StatelessWidget {
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Genie'),
        centerTitle: true,
        backgroundColor: CustomColors.colorAppBar,
        automaticallyImplyLeading: false,
        leading: InkWell(
            onTap: () => Navigator.pushNamed(context, '/account'),
            child: Icon(Icons.menu)),
        actions: <Widget>[
          Icon(Icons.search),
          SizedBox(
            width: 20.0,
          ),
          Icon(Icons.short_text),
          SizedBox(
            width: 20.0,
          ),
        ],
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(child: _itemHeader(text: 'CATEGORIAS')),
          SliverToBoxAdapter(child: 
          _categoriasList(),),
          SliverToBoxAdapter(child: _itemHeader(text: 'DESCUENTOS', secondText: 'Ordernar por')),
          SliverToBoxAdapter(child: _descuentoCard(),),
          SliverToBoxAdapter(child: _descuentoCard(),),
          SliverToBoxAdapter(child: _descuentoCard(),),
          SliverToBoxAdapter(child: _descuentoCard(),),
          SliverToBoxAdapter(child: _descuentoCard(),),
          SliverToBoxAdapter(child: _descuentoCard(),),
          SliverToBoxAdapter(child: _descuentoCard(),),

        ],
      ),
    );
  }

  Widget _itemHeader({String text, Color color = Colors.transparent, String secondText = 'Ver todas'}) {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0, left: 10.0, bottom: 10.0),
      child: Container(
        color: color,
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    text,
                    style: TextStyle(color: CustomColors.colorAppBar, fontSize: 20.0),
                  ),
                  Text(
                    secondText,
                    style: TextStyle(color: CustomColors.colorPrimaryOrange, fontSize: 18.0),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _categoriasList() {
    return Container(
      color: Colors.white,
      height: 120.0,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 10.0),
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (context, index) {
          return _card(categories[index]['name'], categories[index]['image']);
        },
      ),
    );
  }

  Widget _card(String title, String image) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),

      width: 170.0,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        semanticContainer: true,
        elevation: 5.0,
        child: Stack(
          children: <Widget>[
            Image.asset(
              image,
              fit: BoxFit.fill,
              width: double.infinity,
              height: double.infinity,
            ),
            Container(color: Colors.black26,),
            Positioned(
              left: 15.0,
              bottom: 10.0,
              child: Text(
                title,
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }

 Widget _descuentoCard() {
    final _height = 140.0;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      margin: EdgeInsets.symmetric(vertical: 5.0),
      height: _height,
      child: Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0)),
              child: Image.asset(
                'assets/images/oferta_de_hoy.png',
                fit: BoxFit.fill,
                width: _height,
              ),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Entretenimiento'),
                  Text(
                    'Cirque du Solei',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    '20% DE DESCUENTO',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  Expanded(child: Container()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('8,1 km'),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: CustomColors.colorprimaryGrey),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text('Venc: 29/11'),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
