import 'package:flutter/material.dart';
import 'package:genie/src/components/standart_button_component.dart';
import 'package:genie/src/utils/colors/custom_colors.dart';

class InitialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        color: Colors.black,
        image: DecorationImage(
            image: AssetImage("assets/images/millenials.jpg"),
            alignment: Alignment(0.3,  0),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.5), BlendMode.dstATop)),
      ),
      child: SafeArea(
              child: Stack(
          children: <Widget>[
            _logo(),
            _buttons(context),
          ],
        ),
      ),
    ));
  }

  Widget _logo() {
    return Center(
      child: Align(
        alignment: Alignment(0, -0.1),
              child: Text(
          'Genie',
          style: TextStyle(color: Colors.white, fontSize: 40.0),
        ),
      ),
    );
  }


  Widget _buttons(BuildContext context){
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        StandartButtonComponent(color: CustomColors.colorPrimaryOrange, text: 'INICIAR SESIÓN', route: '/signin',),
        SizedBox(height: 25.0,),
        StandartButtonComponent(color: CustomColors.colorPrimaryBlue, text: 'NUEVO USUARIO', route: '/signup'),
        SizedBox(height: 25.0,)
      ],
    );
  }

 

}
