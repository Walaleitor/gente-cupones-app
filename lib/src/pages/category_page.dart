import 'package:flutter/material.dart';
import 'package:genie/src/components/standart_button_component.dart';
import 'package:genie/src/data/data.dart';
import 'package:genie/src/utils/colors/custom_colors.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  

  bool isMartketAll = false;

  void _marketAll() {
    if (!isMartketAll) {
      for (var category in categorys) {
        category['isMarket'] = true;
      }
      isMartketAll = true;
    } else {
       for (var category in categorys) {
        category['isMarket'] = false;
      }
      isMartketAll = false;
    }
    
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: CustomColors.colorAppBar,
          title: Text('Categorías'),
        ),
        body: Stack(
          children: <Widget>[
            _gridCards(size, context),
            _header(size),
            _button(size)
          ],
        ));
  }

  Widget _header(Size size) {
    return Column(
      children: <Widget>[
        Container(
          height: 50.0,
          child: Align(
              alignment: Alignment(-0.5, 0),
              child: Text(
                'Selecciona las categorías que desees',
                style: TextStyle(fontSize: 18.0),
              )),
        ),
        InkWell(
          onTap: () {
            setState(() {
              _marketAll();
            });
          },
          child: Container(
            width: size.width * 0.93,
            height: 60.0,
            child: Card(
              elevation: 10.0,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.done_all,
                      color: CustomColors.colorAppBar,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      'SELECCIONAR TODAS',
                      style: TextStyle(color: CustomColors.colorAppBar, fontSize: 18.0),
                    ),
                    Expanded(child: Container()),
                    Text('(${categorys.length})',
                        style: TextStyle(color: CustomColors.colorAppBar, fontSize: 18.0))
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _gridCards(Size size, BuildContext context) {
    final double itemWidth = size.width / 2;
    final double itemHeight = 120.0;
    return Padding(
      padding: const EdgeInsets.only(top: 108.0, left: 10.0, right: 10.0),
      child: Container(
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: (itemWidth / itemHeight),
            crossAxisCount: 2,
          ),
          itemCount: categorys.length,
          itemBuilder: (context, index) {
            return InkWell(
                onTap: () {
                  setState(() {
                    categorys[index]['isMarket'] =
                        !categorys[index]['isMarket'];
                  });
                },
                child: _card(categorys[index]['category'],
                    categorys[index]['isMarket']));
          },
        ),
      ),
    );
  }

  Widget _card(String title, bool isMarket) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
      height: 20.0,
      child: Card(
        color: isMarket ? CustomColors.colorPrimaryOrange : CustomColors.colorCategoryCard,
        elevation: 5.0,
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 15.0,
              bottom: 15.0,
              child: Text(
                title,
                style: TextStyle(
                    fontSize: 16.0,
                    color: isMarket ? Colors.white : Colors.black),
              ),
            ),
            Positioned(
                right: 5.0,
                top: 5.0,
                child: Visibility(
                  visible: isMarket,
                  child: Icon(
                    Icons.done,
                    color: Colors.white,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _button(Size size) {
    return Positioned(
      bottom: 20.0,
      child: Container(
        width: size.width,
        child: StandartButtonComponent(
          text: 'BUSCAR EN CATEGORIAS',
          route: '/loading',
          color: CustomColors.colorPrimaryBlue,
        ),
      ),
    );
  }
}
