import 'package:flutter/material.dart';
import 'package:genie/src/data/data.dart';
import 'package:genie/src/utils/colors/custom_colors.dart';

class DashboardPage extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Genie'),
        centerTitle: true,
        backgroundColor: CustomColors.colorAppBar,
        automaticallyImplyLeading: false,
        leading: InkWell(
            onTap: () => Navigator.pushNamed(
                  context,
                  '/account',
                ),
            child: Icon(Icons.menu)),
        actions: <Widget>[
          InkWell(
            onTap: () => Navigator.pushNamed(context, '/search'),
            child: Icon(Icons.search)),
          SizedBox(
            width: 20.0,
          ),
          InkWell(
              onTap: () => Navigator.pushNamed(context, '/discounts'),
              child: Icon(Icons.short_text)),
          SizedBox(
            width: 20.0,
          ),
        ],
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: _header(),
          ),
          SliverToBoxAdapter(
            child: _itemHeader(text: 'CATEGORIAS'),
          ),
          _categoriesGrid(size),
          _sliverSizedBox(),
          SliverToBoxAdapter(
            child: _itemHeader(text: 'BENEFICIOS', color: Colors.white),
          ),
          SliverToBoxAdapter(
            child: _beneficiosList()),
          _sliverSizedBox(),
          SliverToBoxAdapter(
            child: _itemHeader(text: 'NUEVOS BENEFICIOS', color: Colors.white),
          ),
          SliverToBoxAdapter(child: _beneficiosList()),
          _sliverSizedBox(),
        ],
      ),
    );
  }

  Widget _header() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(right: 10.0, left: 10.0, bottom: 25.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Align(
                alignment: Alignment(-0.9, 0),
                child: Text(
                  'OFERTA DE HOY',
                  style: TextStyle(color: CustomColors.colorAppBar, fontSize: 20.0),
                ),
              ),
            ),
            _descuentoCard()
          ],
        ),
      ),
    );
  }

  Widget _descuentoCard() {
    final _height = 140.0;

    return Container(
      height: _height,
      child: Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Row(
          children: <Widget>[
            ClipRRect(
              
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0)),
              child: Image.asset(
                'assets/images/oferta_de_hoy.png',
                fit: BoxFit.fill,
                width: _height,
              ),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Entretenimiento'),
                  Text(
                    'Cirque du Solei',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    '20% DE DESCUENTO',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  Expanded(child: Container()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('8,1 km'),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: CustomColors.colorprimaryGrey),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text('Venc: 29/11'),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

  Widget _itemHeader({String text, Color color = Colors.transparent}) {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0, left: 10.0, bottom: 10.0),
      child: Container(
        color: color,
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    text,
                    style: TextStyle(color: CustomColors.colorAppBar, fontSize: 20.0),
                  ),
                  Text(
                    'Ver todas',
                    style: TextStyle(color: CustomColors.colorPrimaryOrange, fontSize: 18.0),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _categoriesGrid(Size size) {
    final double itemWidth = size.width / 2;
    final double itemHeight = 120.0;
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: (itemWidth / itemHeight),
        crossAxisCount: 2,
      ),
      delegate: SliverChildBuilderDelegate((context, index) {
        return _card(categories[index]['name'], categories[index]['image']);
      }, childCount: 6),
    );
  }

  Widget _card(String title, String image) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
      height: 20.0,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        semanticContainer: true,
        elevation: 5.0,
        child: Stack(
          children: <Widget>[
            Image.asset(
              image,
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
            ),
            Container(color: Colors.black26,),
            Positioned(
              left: 15.0,
              bottom: 10.0,
              child: Text(
                title,
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _beneficiosCard() {
    return Container(
      height: 310.0,
      width: 170.0,
      child: Card(
        elevation: 5.0,
        child: Column(
          children: <Widget>[
            Container(
              height: 110.0,
              child: Image.asset(
                'assets/images/estaciones_de_gas.png',
                fit: BoxFit.cover,
              ),
            ),
            Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Align(
                        alignment: Alignment(-0.8, 0),
                        child: Text(
                          'Heladeria Fredo',
                        )),
                    SizedBox(
                      height: 2.0,
                    ),
                    Align(
                        alignment: Alignment(-0.8, 0),
                        child: Text(
                          'Deliciosas Cremas',
                        )),
                    SizedBox(height: 10.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          '0.3 km',
                          style: TextStyle(color: CustomColors.colorPrimaryOrange),
                        ),
                        Text('4.3', style: TextStyle(color: CustomColors.colorPrimaryOrange))
                      ],
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }

  Widget _beneficiosList() {
    return Container(
      color: Colors.white,
      height: 200.0,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 10.0,),
        scrollDirection: Axis.horizontal,
        itemCount: 10,
        itemBuilder: (context, index) {
          return _beneficiosCard();
        },
      ),
    );
  }

  Widget _sliverSizedBox() {
    return SliverToBoxAdapter(
      child: SizedBox(
        height: 20.0,
      ),
    );
  }
}
