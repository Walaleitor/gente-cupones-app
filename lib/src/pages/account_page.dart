import 'package:flutter/material.dart';
import 'package:genie/src/utils/colors/custom_colors.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
          child: Column(
        children: <Widget>[
          _header(size, context),
          _item(size, 'Inicio'),
          _item(size, 'Mapa'),
          _item(size, 'Favoritos'),
          _item(size, 'Vincular Cuentas'),
          _beneficioSorpresa(size),
          _footer(size)
        ],
      )),
    );
  }

  Widget _header(Size size, BuildContext context) {
    return Container(
      height: size.height * 0.25,
      color: CustomColors.colorPrimaryBlue,
      child: SafeArea(
        child: Stack(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(top: 30.0, left: 25.0, right: 25.0),
              child: Row(
                children: <Widget>[
                  Container(
                    height: size.height * 0.12,
                    width: size.height * 0.12,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('assets/images/avatar.jpg'))),
                  ),
                  SizedBox(
                    width: 25.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Juan José Gutierrez',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          )),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        'jjgutierrez@gmail.com',
                        style: TextStyle(color: Colors.white, fontSize: 17.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        'EDITAR',
                        style: TextStyle(
                            color: CustomColors.colorPrimaryOrange, fontSize: 20.0),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Positioned(
                top: 10.0,
                right: 25.0,
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.clear,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _item(Size size, String text) {
    return Container(
      height: size.height * 0.107,
      child: Align(
        alignment: Alignment(-0.8, 0.0),
        child: Text(
          text,
          style: TextStyle(fontSize: 25.0),
        ),
      ),
    );
  }

  Widget _beneficioSorpresa(Size size) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        height: size.height * (0.107 * 2) - 50,
        decoration: BoxDecoration(
            color: CustomColors.colorprimaryGrey,
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Row(
          children: <Widget>[
            Flexible(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '¡Sorpresa!',
                        style: TextStyle(fontSize: 22.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text('Desbloquea un beneficio por dia',
                          style: TextStyle(fontSize: 15.0)),
                      SizedBox(
                        height: 5.0,
                      ),
                      // Container(
                      //   width: 100,
                      //   height: 50.0,
                      //   decoration: BoxDecoration(
                      //       color: colorAppBar,
                      //       borderRadius:
                      //           BorderRadius.all(Radius.circular(10.0))),
                      //   child: Center(
                      //     child: Text(
                      //       'ABRIR',
                      //       style:
                      //           TextStyle(color: Colors.white, fontSize: 18.0),
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                )),
            Flexible(
                flex: 2,
                child: Container(
                  child: Image.asset(
                    'assets/illustrations/open-doodles-unboxing.png',
                  ),
                )),
          ],
        ),
      ),
    );
  }

  Widget _footer(Size size) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        height: size.height * 0.107,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              'Cerrar sesión',
              style: TextStyle(color: CustomColors.colorPrimaryOrange, fontSize: 20.0),
            ),
            Text(
              'Ayuda',
              style: TextStyle(color: CustomColors.colorPrimaryOrange, fontSize: 20.0),
            ),
          ],
        ));
  }
}
