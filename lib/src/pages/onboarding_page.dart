import 'package:flutter/material.dart';

import 'package:genie/src/utils/colors/custom_colors.dart';

class OnboardingPage extends StatefulWidget {
  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  int currentPage = 0;
  int finalPage = 2;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    final List<Widget> introWidgetsList = <Widget>[
      _introScreens('Recibe todos los beneficios en tu movil', 'assets/illustrations/open-doodles-selfie.png', size),
      _introScreens('Consulta por beneficios en tu zona', 'assets/illustrations/open-doodles-sleek.png', size),
      _introScreens('Centraliza todas tus tarjetas en un solo lugar', 'assets/illustrations/open-doodles-clumsy.png', size),
    ];

    void getChangedPageAndMoveBar(int page) {
      currentPage = page;
      setState(() {});
    }

    return Scaffold(
        backgroundColor: CustomColors.colorPrimaryOrange,
        body: Stack(
          children: <Widget>[
            PageView.builder(
                onPageChanged: (int page) {
                  getChangedPageAndMoveBar(page);
                },
                physics: ClampingScrollPhysics(),
                itemCount: introWidgetsList.length,
                itemBuilder: (context, index) {
                  return introWidgetsList[index];
                }),
            Align(
              alignment: Alignment(0, -0.85),
              child: _indicators(introWidgetsList),
            ),
            Visibility(
                child: Align(
              alignment: Alignment(0.0, 0.9),
              child: InkWell(
                onTap: () {
                  Navigator.pushReplacementNamed(context, '/category');
                },
                child: Container(
                    child: Text(
                  currentPage == finalPage ? 'Terminar' : 'Saltar',
                  style: TextStyle(fontSize: 20.0, color: CustomColors.colorPrimaryBlue),
                )),
              ),
            ))
          ],
        ));
  }

  Widget _introScreens(String text, String image, Size size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            text,
            style: TextStyle(fontSize: 30.0, color: Colors.white),
            textAlign: TextAlign.center,
          ),
        )),
        SizedBox(
          height: 100.0,
        ),
        Center(
          child: Image(
            height: size.height * 0.5,
              image:
                  AssetImage(image)),
        ),
      ],
    );
  }

  Widget _indicators(List widgets) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: widgets.asMap().entries.map((widget) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: Container(
              height: 10.0,
              width: 60.0,
              decoration: BoxDecoration(
                  color:
                      widget.key == currentPage ? Colors.white : Colors.white30,
                  borderRadius: BorderRadius.circular(10.0)),
            ),
          );
        }).toList());
  }
}
