import 'dart:ui';
import 'package:genie/src/utils/colors/hex_color.dart';

class CustomColors {
  static Color colorPrimaryOrange = HexColor("#FF6968");
  static Color colorPrimaryBlue = HexColor("#1446B5");
  static Color colorAppBar = HexColor("#293263");
  static Color colorCategoryCard = HexColor('#F2F2F2');
  static Color colorprimaryGrey = HexColor('#E5E5E5');
  static Color colorSearchBar = HexColor('#707070');
}
