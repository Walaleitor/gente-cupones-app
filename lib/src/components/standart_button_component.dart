import 'package:flutter/material.dart';

class StandartButtonComponent extends StatelessWidget {
  final Color color;
  final String text;
  final String route;

  StandartButtonComponent(
      {this.color, @required this.text, @required this.route});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: InkWell(
        onTap: () {
          if(route == '/back')  return Navigator.pop(context);
           Navigator.pushNamed(context, route);
        },
        child: Container(
          height: size.height * 0.065,
          width: size.width * 0.9,
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          child: Center(
              child: Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: 18.0),
          )),
        ),
      ),
    );
  }
}
