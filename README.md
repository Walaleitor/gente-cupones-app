# Genie Demo

## Getting Started

Esta aplicacion es solo un demo.

La app fue desarrollada bajo la version 1.12.1 de Flutter SDK

Fue probada en los siguentes dispositivos:

- iphone X
- iphone 11
- iphone 8
- Pixel3 API 28

Para correr la app debe conectar un dispositivo fisico o levantar una maquina virtual y correr el comando `flutter run`
